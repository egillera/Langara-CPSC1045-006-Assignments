/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world"); 
//Get the input element
//Get their values and store them in variables

var rotation = 0;

function Update() {
rotation = +document.querySelector("#range").value;
var range = document.querySelector("#collection")
let rotateString = "rotate(" + rotation + " 100 180)"
console.log(rotateString);
collection.setAttribute("transform", rotateString);
let Xbox = document.querySelector("#X");
let Ybox = document.querySelector("#Y");
console.log(Xbox);
console.log(Ybox);
let translateString = "translate(" + Xbox.value + "1 1" + Ybox.value+")";
console.log(translateString)
let transformString = translateString + "" + rotateString;
console.log(transformString);
collection.setAttribute("transform", transformString);
}

 window.Update = Update

 function change(){
console.log("Good");
let extendout = document.querySelector("#range2");
let extendin = document.querySelector("#range3");
let movementx = document.querySelector("#xmov");
let movementy = document.querySelector("#ymov");
let outextend = Number(extendout.value);
let inextend = Number(extendin.value);

let x1 = outextend * Math.cos(0)
let x2 = inextend * Math.cos(0.76)
let x3 = outextend * Math.cos (1.57)
let x4 = inextend * Math.cos(2.36)
let x5 = outextend * Math.cos(3.14)
let x6 = inextend * Math.cos(3.92)
let x7 = outextend * Math.cos(4.71)
let x8 = inextend * Math.cos(5.5)

let y1 = outextend * Math.sin(0)
let y2 = inextend * Math.sin(0.76)
let y3 = outextend * Math.sin(1.57)
let y4 = inextend * Math.sin(2.36)
let y5 = outextend * Math.sin(3.14)
let y6 = inextend * Math.sin(3.92)
let y7 = outextend * Math.sin(4.71)
let y8 = inextend * Math.sin(5.5)


let xmov = Number(movementx.value)
let ymov = Number(movementy.value)
let translatestar = "translate("+xmov.value + " " + ymov.value+")";
let pointsign = (x1 + " " + y1 + " " + x2 + " " + y2 + " " + x3 + " " + y3 + " " + x4 + " " + y4 + " " + x5 + " " + y5 + " " + x6 + " " + y6 + " " + x7 + " " + y7 + " " + x8 + " " + y8)
star.setAttribute("points", pointsign);
console.log(pointsign)

// star.setAttribute("transform", translatestar);
}
 window.change = change 

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBO0FBQ0E7QUFDQSwyQjtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esd0IiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LmpzXCIpO1xuIiwiLy9UaGlzIGlzIHRoZSBlbnRyeSBwb2ludCBKYXZhU2NyaXB0IGZpbGUuXHJcbi8vV2VicGFjayB3aWxsIGxvb2sgYXQgdGhpcyBmaWxlIGZpcnN0LCBhbmQgdGhlbiBjaGVja1xyXG4vL3doYXQgZmlsZXMgYXJlIGxpbmtlZCB0byBpdC5cclxuY29uc29sZS5sb2coXCJIZWxsbyB3b3JsZFwiKTsgXHJcbi8vR2V0IHRoZSBpbnB1dCBlbGVtZW50XHJcbi8vR2V0IHRoZWlyIHZhbHVlcyBhbmQgc3RvcmUgdGhlbSBpbiB2YXJpYWJsZXNcclxuXHJcbnZhciByb3RhdGlvbiA9IDA7XHJcblxyXG5mdW5jdGlvbiBVcGRhdGUoKSB7XHJcbnJvdGF0aW9uID0gK2RvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcmFuZ2VcIikudmFsdWU7XHJcbnZhciByYW5nZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjY29sbGVjdGlvblwiKVxyXG5sZXQgcm90YXRlU3RyaW5nID0gXCJyb3RhdGUoXCIgKyByb3RhdGlvbiArIFwiIDEwMCAxODApXCJcclxuY29uc29sZS5sb2cocm90YXRlU3RyaW5nKTtcclxuY29sbGVjdGlvbi5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIiwgcm90YXRlU3RyaW5nKTtcclxubGV0IFhib3ggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI1hcIik7XHJcbmxldCBZYm94ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNZXCIpO1xyXG5jb25zb2xlLmxvZyhYYm94KTtcclxuY29uc29sZS5sb2coWWJveCk7XHJcbmxldCB0cmFuc2xhdGVTdHJpbmcgPSBcInRyYW5zbGF0ZShcIiArIFhib3gudmFsdWUgKyBcIjEgMVwiICsgWWJveC52YWx1ZStcIilcIjtcclxuY29uc29sZS5sb2codHJhbnNsYXRlU3RyaW5nKVxyXG5sZXQgdHJhbnNmb3JtU3RyaW5nID0gdHJhbnNsYXRlU3RyaW5nICsgXCJcIiArIHJvdGF0ZVN0cmluZztcclxuY29uc29sZS5sb2codHJhbnNmb3JtU3RyaW5nKTtcclxuY29sbGVjdGlvbi5zZXRBdHRyaWJ1dGUoXCJ0cmFuc2Zvcm1cIiwgdHJhbnNmb3JtU3RyaW5nKTtcclxufVxyXG5cclxuIHdpbmRvdy5VcGRhdGUgPSBVcGRhdGVcclxuXHJcbiBmdW5jdGlvbiBjaGFuZ2UoKXtcclxuY29uc29sZS5sb2coXCJHb29kXCIpO1xyXG5sZXQgZXh0ZW5kb3V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNyYW5nZTJcIik7XHJcbmxldCBleHRlbmRpbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjcmFuZ2UzXCIpO1xyXG5sZXQgbW92ZW1lbnR4ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN4bW92XCIpO1xyXG5sZXQgbW92ZW1lbnR5ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiN5bW92XCIpO1xyXG5sZXQgb3V0ZXh0ZW5kID0gTnVtYmVyKGV4dGVuZG91dC52YWx1ZSk7XHJcbmxldCBpbmV4dGVuZCA9IE51bWJlcihleHRlbmRpbi52YWx1ZSk7XHJcblxyXG5sZXQgeDEgPSBvdXRleHRlbmQgKiBNYXRoLmNvcygwKVxyXG5sZXQgeDIgPSBpbmV4dGVuZCAqIE1hdGguY29zKDAuNzYpXHJcbmxldCB4MyA9IG91dGV4dGVuZCAqIE1hdGguY29zICgxLjU3KVxyXG5sZXQgeDQgPSBpbmV4dGVuZCAqIE1hdGguY29zKDIuMzYpXHJcbmxldCB4NSA9IG91dGV4dGVuZCAqIE1hdGguY29zKDMuMTQpXHJcbmxldCB4NiA9IGluZXh0ZW5kICogTWF0aC5jb3MoMy45MilcclxubGV0IHg3ID0gb3V0ZXh0ZW5kICogTWF0aC5jb3MoNC43MSlcclxubGV0IHg4ID0gaW5leHRlbmQgKiBNYXRoLmNvcyg1LjUpXHJcblxyXG5sZXQgeTEgPSBvdXRleHRlbmQgKiBNYXRoLnNpbigwKVxyXG5sZXQgeTIgPSBpbmV4dGVuZCAqIE1hdGguc2luKDAuNzYpXHJcbmxldCB5MyA9IG91dGV4dGVuZCAqIE1hdGguc2luKDEuNTcpXHJcbmxldCB5NCA9IGluZXh0ZW5kICogTWF0aC5zaW4oMi4zNilcclxubGV0IHk1ID0gb3V0ZXh0ZW5kICogTWF0aC5zaW4oMy4xNClcclxubGV0IHk2ID0gaW5leHRlbmQgKiBNYXRoLnNpbigzLjkyKVxyXG5sZXQgeTcgPSBvdXRleHRlbmQgKiBNYXRoLnNpbig0LjcxKVxyXG5sZXQgeTggPSBpbmV4dGVuZCAqIE1hdGguc2luKDUuNSlcclxuXHJcblxyXG5sZXQgeG1vdiA9IE51bWJlcihtb3ZlbWVudHgudmFsdWUpXHJcbmxldCB5bW92ID0gTnVtYmVyKG1vdmVtZW50eS52YWx1ZSlcclxubGV0IHRyYW5zbGF0ZXN0YXIgPSBcInRyYW5zbGF0ZShcIit4bW92LnZhbHVlICsgXCIgXCIgKyB5bW92LnZhbHVlK1wiKVwiO1xyXG5sZXQgcG9pbnRzaWduID0gKHgxICsgXCIgXCIgKyB5MSArIFwiIFwiICsgeDIgKyBcIiBcIiArIHkyICsgXCIgXCIgKyB4MyArIFwiIFwiICsgeTMgKyBcIiBcIiArIHg0ICsgXCIgXCIgKyB5NCArIFwiIFwiICsgeDUgKyBcIiBcIiArIHk1ICsgXCIgXCIgKyB4NiArIFwiIFwiICsgeTYgKyBcIiBcIiArIHg3ICsgXCIgXCIgKyB5NyArIFwiIFwiICsgeDggKyBcIiBcIiArIHk4KVxyXG5zdGFyLnNldEF0dHJpYnV0ZShcInBvaW50c1wiLCBwb2ludHNpZ24pO1xyXG5jb25zb2xlLmxvZyhwb2ludHNpZ24pXHJcblxyXG4vLyBzdGFyLnNldEF0dHJpYnV0ZShcInRyYW5zZm9ybVwiLCB0cmFuc2xhdGVzdGFyKTtcclxufVxyXG4gd2luZG93LmNoYW5nZSA9IGNoYW5nZSAiXSwic291cmNlUm9vdCI6IiJ9